#!/usr/bin/env python
# -*- coding: utf-8 -*-
""" Worker code for producing and consuming RabbitMQ messages
"""
import pika
import json
import time
import yaml

#open configuration file
with open("config.yml", 'r') as ymlfile:
    cfg = yaml.load(ymlfile)

#Declare rabbitmq connections
credentials = pika.PlainCredentials(cfg['rabbitUser'], cfg['rabbitPsw'])
parametersConsume = pika.ConnectionParameters(cfg['receiveFrom'], cfg['port'], \
                                '/', credentials, retry_delay=15, \
                                connection_attempts=5)
parametersProduce = pika.ConnectionParameters(cfg['produceTo'], cfg['port'], \
                                '/', credentials, retry_delay=15, \
                                connection_attempts=5)

#Start consuming connection
connectionConsume = pika.BlockingConnection(parametersConsume)
channelConsume = connectionConsume.channel()
#Declare durable queue
channelConsume.queue_declare(queue=cfg['queue'], durable=True)

#Function for getting current time
def timeStamp():
    """Function for generating timestamp"""
    stamp = time.ctime(int(time.time()))
    return stamp

#Function for producing messages
def produce(item):
    """ Function for producing a message to a message queue"""
    #Start producing connection
    connectionProduce = pika.BlockingConnection(parametersProduce)
    channelProduce = connectionProduce.channel()
    channelProduce.queue_declare(queue=cfg['queue'], durable=True)
    message = json.dumps(item)
    #Publish message to queue
    channelProduce.basic_publish(exchange='', routing_key=cfg['queue'], \
                                body=message, \
                                properties=pika.BasicProperties( \
                                    delivery_mode=2, \
                                    content_type='application/json'))
    connectionProduce.close()
    print " [*] " + timeStamp() + " Done producing"

#Function for consuming messages
def callback(ch, method, properties, body):
    """ Function for consuming messages from message queue"""
    print " [*] " + timeStamp() + " Accepted job"
    data = json.loads(body)
    sleep_tag = int(data["jobType"])
    time.sleep(sleep_tag)
    stamptime = timeStamp()
    data["stamp"].append({cfg['worker']:stamptime})
    channelConsume.basic_ack(delivery_tag=method.delivery_tag)
    print " [*] " + timeStamp() + " Done consuming, starting producing"
    produce(data)
    print " [*] " + timeStamp() + " Waiting for next job"

print " [*] " + timeStamp() + " Waiting for job"

channelConsume.basic_consume(callback, queue=cfg['queue'])
channelConsume.start_consuming()
